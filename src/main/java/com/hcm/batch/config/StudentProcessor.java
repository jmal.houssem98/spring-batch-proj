package com.hcm.batch.config;

import com.hcm.batch.student.Student;
import org.springframework.batch.item.ItemProcessor;

public class StudentProcessor implements ItemProcessor<Student,Student> {
    @Override
    public Student process(Student student) {
        //Business logic here
        student.setId(null);
        return student;
    }


}
